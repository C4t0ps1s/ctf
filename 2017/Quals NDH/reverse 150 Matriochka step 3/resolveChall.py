
from pwn import *

refList = []
refList.append("ABzsaFVRIJ7LjNoPQ/ZTUMngYihlcde+XfStkO39pqbrDGHvwy5C012m4xK68WEu")
refList.append("uBiraFgQIC/LD1OR7PYTsMVnZJhlcdH+fqXSkE3vp5btjGoAwy6z098e4xKU2WNm")
refList.append("eKzjbkgmBIP0DERO/7XTsFVnZqh1odQ+fJYrGa3wypltUWAMv5xiL98u46CS2cNH")
refList.append("eKzjbQ+3/BJ0DRaZ7IqTL9VgYNh1o2kwfPXrSO8npylCUWH456xiFsmuMvtGdcEA")
refList.append("e/XU1G+7BKTiDbaYI3xjLVcfNEh60mHwgPqSQR8nypOJCWZ45lzdFs2AvMtro9ku")
refList.append("MAXU6Q+2BKTdDuwYI3xrLVjaENP1om5fghzpGkb4qyOtCWZHlnS0Fs7ev/iJ98Rc")
refList.append("MKXN1+W2BAh0Uk5EITC3sF8gxat6oLfwPYSDGRbly4JOdQcvnqzpVm7e/Hir9juZ")
refList.append("MKaU1GWeBXhpQRwgITADsj82xCtN0m+lPYzrfbc5yqJ36dZ/n4O9VL7kHviSoFuE")
refList.append("MHN6f+QeBChVRbuxTXISsF8Pa7t9Em52qlvz1UW/yYJ3OcdGn4jApLgkrKiDo0wZ")
refList.append("kMN9IbLTBCtVcQuxeX6opSW2alhfE+5Pqyv1Dd8G47JsROwn/r3AFmgHKYizj0UZ")
refList.append("8YC9I5OeNBiSRdb2lZ6juwHxaXVf0+m4y7v1DQWGPqJpcsEk/r3AhFgnKMtzoLUT")
refList.append("nkMvQ5seNBZSRcGUli96FVgxXqwf0+EbyajDzdW/PKJpIL24t7CAhuO83Yr1oHmT")
refList.append("nkAfQ50eNiadcRmUlBj6hLg/qXSvoWExyC9IzHOtPK7pDFV3brYZ2u+84MJ1wsGT")
refList.append("8JYpiz5eATaZDOxUlX9jhstyqBKQLw3m/S1dFGHgPCfvc0Vnbr7I2W+E4Mk6uoRN")
refList.append("8k7jG0y+NTaZdH35lBpuvVtUqXKIQwmPS/1Di2cnxCf9OozgbMYLsWeE4rJFh6RA")
refList.append("8JB9Gh6+NXaKoWAplUTwvdy5q7ZQVmiP/SYDR2cMx1LOjt0gbfCns3eE4rkuFIHz")

fatStr = ""
fatStr += "mCtOmdh0mEW3idU4lRd9QMd2KEdRZw7AmRdOTelCl213anXSm+lgVRteoR3SV3UYm"
fatStr += "sd47EmGZdmyUwJhvvdEU+7eochqivWcQ9NgUy3dl3SKUhU3oE7vdy31dRdUVn70Ts"
fatStr += "mEwGXLoMS37wXpy+WNZRjOT+dRQD8bV6WNVEJLZ3d3iTI7mRmWlstrd+hUoBIqmsm"
fatStr += "qmdS+dG1Bwh7oQdd9QDWdZEdUmcf5lRVClMVbTG3UV6IhVBtBTMJDT67Bdcf7mMdE"
fatStr += "Uh72Ksred37p79XEQMJ2oDhRQMm0m98CWehhd6WsU3WYvddElsXrdGXbdvSWmsdBy"
fatStr += "yfeWMSRUsX+lDXRwv2LUeXtd2W5VyrqwGS2WcrsZwXo76rBTcfnlDdqm62nTy1vWd"
fatStr += "SGoMSewhU476Wvd5vO76mNdCFOmRvCUvt0T6XUdMVOTE7xds2nlRIBdRXWV5dvmdG"
fatStr += "OlcXedsWRv5WWi2homemKZw7rmvfvwEJ+dGmsUhUedc11dEd2VRmNmRXvTeWyQh7n"
fatStr += "wvfEdetfVBtviMU1dR1yVn7rlRvbU5VbAGd3U37av57Biyf0dGfbdRXv7+ogVybCK"
fatStr += "CtUUdlsT+leWv2rmEhfVsXAmEUCihSdT6f3VCINd2VbqRXnZ5XKoGXyoElCWvXLoM"
fatStr += "S3VsWpyGm9VDWeKsdB7vWYmMWqUdh5T9hEVEmam+dNi5hGlMSKoYIGo3d9TMh0l2S"
fatStr += "fdEm4mEReQ+d+dM7qZR/OmRfv7GjCQhmtaejEmRmWZ57ho67RQ2l3vd7EZcfdT+We"
fatStr += "a3SfyhVgVDWdlEdUQRNCm9J9wEmrd+1UV37fyhm9dhhhdGXKQRSWo3d47sShlRWtZ"
fatStr += "vX+VBty7dhslEdXZyX5vwXOV3SdT+1sUhU7lc2gQRRbmCISUvr7oEqCqwWdKESjoD"
fatStr += "NEdRd9y5drdRWXZw7CvydqVEJ+vd2KqnXMoE7Ww+vOyGIbohhCQ6rNT272KsreUTI"
fatStr += "cTer+VEdhlR1UQRNOmcdEy2S+d6tXUCIhyhVgQMlOle7bmhWnvd7ETMUewv1e75mo"
fatStr += "762NQMJ2oMdN7TIvv5W5UsNCQhRnmvrYTyrBi57nQEWUUES5lMRCih72qD2yivWK7"
fatStr += "GUOA5dLZyXtdMS7vyWgUvjCTGmtaetoV+WWw6fhUsfbaEmSmGVgW3SDlsWed37nT+"
fatStr += "7RdEmLd2mXqES5dRvCWsXylD2KZDJMQ5d1yyfymEWNd5SYm9XEm5hyo+WUUy2oV9J"
fatStr += "O7wWdZehBasX4Q9XNU5J+dh23Z27+me4bd+dhdh1qdvS7mMlgTRtelDJUmhUpV+2Y"
fatStr += "AwWdT+dXiTI1dRVC7GX0T92XWeILmYtElEd2ms3BmRS7V5JglsS5Ke2eUwJZT37Ew"
fatStr += "MJ2Zymy7vWAm2my7hSnTh2KqnX7ThVbdyfnlDUtocf7TsUyVv21WMWBVeIcy+UWd5"
fatStr += "JnmE7ydYI1mBtvKeU1Z6XUVnXov5lgqcfGd67qdvW+o3d9leU0lR1jUwJRThdvd2h"
fatStr += "5ms1fwGSryRfBWe31msWBo+mRT+Rgo2h2le7toRXdmyxC73h0o62Um5mZmnJUVRtG"
fatStr += "d9dNVeXC7BjCKelCQh2bVe2MqvdBidGCoMWUoGXdTe2E7+mGZdmRUefCQdd9o5Vbo"
fatStr += "RUy7vsOmy1EWy3ele7fdMmhV6d9ocfnZ+XUyw727+WEZ2S2w5X375m4V+7v7dhhlR"
fatStr += "dB7vWamydNTeUGAG33asSLvy4ba3qbKEWtQRS0mGUAqMUDleXyUsShvd2xUv2+yBt"
fatStr += "qVn74TBtqwy3dT6X3UCIfVhVbZ5ULZnmSU57nVGfyw633oR1BZ2hCq5WNUsjfQnhb"
fatStr += "dwJcdYjbw37hm31Eid7fVvbCl3GClRIBqn7OmsdB7E7+WMWEyyI+QyrEyMdLocmsQ"
fatStr += "MmayRdqwE8Odh/ndvS3dM2wyv22lRmBm9X2mMUyqy3DKsfedEmnvwJBy+d+v32fU9"
fatStr += "XaQhmNWGSsWDhXU3SMvTtNw+75d21NdcfYlMRCm5J2KE2foMV3yGmvWEUro2dUQRX"
fatStr += "CmdmBlRShd2dsZd7alM2YAyfnlMWbiw7ZmCtOm2h1wv13aEhCQy29UGt1oRfRZvI5"
fatStr += "mRdOi+J+wwhXZ2Udy+Wvdyfsd+SSds/rTerNUyfdlDXjds/sTEqeQcf1oRfjVn79v"
fatStr += "vUvihS5lM1BUCFO7GmWw6fnoh1Ea3UdoE7yw+7hKsWfyyF3yGdNqMmhl2mXqCIrm5"
fatStr += "WBVsjOU32UdvSLv57gaEmno+WKmRSCmsdB737hohUyanXpl2mBQ9W3wcXtUGSAl2m"
fatStr += "9wsXDWRU3ZM77VvlCo2hhdGtBm2S2mn8Cd37hUn2Km5mR7GUOqsjCvymfZv3nmEW+"
fatStr += "VcUDwMWKas/Om+U4Av22d67UoGXClYt9mvX2wvrEV3SnV6x4w5mnKsfsqs3dmwmgy"
fatStr += "MJdmeW3UvShy9Nb7MJGocUBVeXhmTjCwGt1QE2Um+X476rvV5droRfsUeICvd2gW3"
fatStr += "ShlnhsivWSoEvCQcsCA+XqdRXCV6vCWeU+w22BUwSCvdmNWEU+Ks3S7TICmvdviGX"
fatStr += "1msVnm279T+oew6f0oRtbdvSZV+WEZ2h+wTt3dCIcQddRqcbfQEdRdDJnTnJEw+m+"
fatStr += "ThdbVnX3v57AydhsVRXSyw7dQ9XEUsS+WMSeVsXc79XNTE7yme1XZyS5m3myweUDZ"
fatStr += "w2RVCI3dMWYVR2hy9UKdvSyoEdyZ2S+dBte7dUBTEleVDWemEdbasXYmRvbyRX1dh"
fatStr += "demBFfdRbCVE75dMSqUvSDmed9wGSnWRreUythQwXBdR2+dR1qVeIamcdWWehhT6f"
fatStr += "UmGNEV6d9lE77QsmKdRNfV+ogZRSeZhmKddSflMoe7RtrKndb7TIrTvvCUvt1w223"
fatStr += "7TI3vwX4A57nWMWNdRSLmsmqm27sw52yanXZlc2vA5dLocmUaEm9Q+Uvw3/b792jq"
fatStr += "CI+VwX+dEmhoGIBVn7jmydwqMUedGfKmdURQddNwv2+dR1qoYIAm3mOiGt0Th2KQc"
fatStr += "teTEqeTRXrVMSqUyUYlYt9mMm0o+XUV3mcTCtvmRtDdR1e7wJdmsdOWEJdT61EUwX"
fatStr += "amRVel37hlESEU3Skmy11qc33o2d3a3SodRd9UEm2ZyXqoDJnmRfEwe3dl3dEZ27f"
fatStr += "Q57BaE7odGdbU3UqmvmqwGt2wdUe75VEQ57Eo5JodDmUmGVrTdmOVdS5lMXb7wNEv"
fatStr += "TtNd+75lsISVs3dv5qCVvNOvdmew9JpVGmvQ+dhlsdyVn74QwNCVst+dGIKUCIhQ+"
fatStr += "7WVelCmEWqmRWWQTt9m2/OThdjUwJ4QyrEQ9WeoMdtqES7dM2qy2h+mnJ3iwX4dRf"
fatStr += "1AylOo+StmR/fm2dOmcUslsf3ZcfClM7+Avt0lE7yVs3nm2m9WGSbWM1emh7+WybI"
fatStr += "pl=="

def fake64_e(data, key):

    res = ""
    spl = split(data,3)
    for s in spl:
        k1 = k2 = ""
        k3 = k4 = "="
        while (len(s) < 3):
            s += "\x00"
        ref1 = int(bits_str(s),2) >> 0x12
        ref2 = (int(bits_str(s),2) >> 0xc) & 0x3f
        ref3 = (int(bits_str(s),2) >> 0x6) & 0x3f
        ref4 = ord(s[2]) & 0x3f

        k1 = key[ref1]
        k2 = key[ref2]
        if ref3:
            k3 = key[ref3]
        if ref4:
            k4 = key[ref4]
        res += k1+k2+k3+k4
    return res

def fake64_d(data, key):

    res = ""
    spl = split(data,4)
    for s in spl:
        c3 = c4 = 0
        c1 = key.find(s[0]) << 0x12
        c2 = key.find(s[1]) << 0xc
        if s[2] != '=':
            c3 = key.find(s[2]) << 0x6
        if s[3] != '=':
            c4 = key.find(s[3])

        fin = c1 | c2 | c3 | c4
        tmp = hex(fin)[2:].decode("hex").replace('\x00','')
        res += tmp
    return res

def split(data, size):
    res = []
    inc = 0
    while True:
        tmp  = ""
        if inc + size > len(data):
            if len(data) > inc:
                res.append(data[inc:])
            return res
        for c in range(0,size):
            tmp += data[inc+c]
        res.append(tmp)
        inc += size


def resolveChall():
    aaa = refList[::-1]
    next = fatStr
    for i in range(0,16):
        print "Step "+str(i)+":"

        res = fake64_d(next,aaa[i])
        print res
        next = res

resolveChall()


